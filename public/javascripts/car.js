function Car (colour) {
	this.isRunning = false;
	this.colour = colour;
}
 
Car.prototype.startRunning = function() {
    this.isRunning = true;
	return this.isRunning;
};
